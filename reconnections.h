//-------+---------+---------+---------+---------+---------+---------+--
//       10        20        30        40        50        60        70
//-------+---------+---------+---------+---------+---------+---------+--
// This code quantifies the number of reconnections as a function of time.
// A developement of previous code prepared by K. Sekizawa. 
// Date: 17-October-2018 by Konrad Kobuszewski
// Compile: gcc reconnections.c -O3 -lm -o reconnections
//-------+---------+---------+---------+---------+---------+---------+--




/*
 * This function is counting number of vortices
 * @param
 */
int find_nvor(FILE* fp_in)
{
    int Nvor = 0;
    inom=0; time=tstart+inom*dt-tcollide; t=time/eF;
    do {
      fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &ivor, &x1, &y1, &z1, &x2, &y2, &z2);
      if(i==inom) Nvor=ivor+1;
    } while(i<=inom);
    fseek(fp_in,0,SEEK_SET);
    
    return Nvor;
}

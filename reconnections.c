//-------+---------+---------+---------+---------+---------+---------+--
//       10        20        30        40        50        60        70
//-------+---------+---------+---------+---------+---------+---------+--
// This code quantifies the number of reconnections as a function of time
// Date: 06-July-2018 by Kazuyuki Sekizawa
// Compile: gcc reconnections.c -O3 -lm -o reconnections
//-------+---------+---------+---------+---------+---------+---------+--

#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <complex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <time.h> /* for ctime() */
#include <unistd.h> /* for sleep() */
#include "pca_settings.h"
// #include "pca_macro.h"
#include "pca_utils.h"
#include "pca_io.h"
#include "pca_logger.h"
#include <fftw3.h> /* for FFTW */

//-------+---------+---------+---------+---------+---------+---------+--

#define file_operation(cmd)                                                     \
    { ierr=cmd;                                                                 \
    if(ierr)                                                                    \
    {                                                                           \
        fprintf( stderr , "error: [%d]cannot execute file operation: %s\nError=%d\nExiting!\n" ,ip, #cmd, ierr) ; \
        ierr=-1;                                                                \
        return( EXIT_FAILURE ) ;                                                \
    } }

#define cppmalloc(pointer,size,type)                                            \
    if ( ( pointer = (type *) malloc( (size) * sizeof( type ) ) ) == NULL )     \
    {                                                                           \
        fprintf( stderr , "error: [%d]cannot malloc()! Exiting!\n" , ip ) ;     \
        ierr = -1 ;                                                             \
        return( EXIT_FAILURE ) ;                                                \
    }

//#define WORK_IN_ROTATING_FRAME
#define OMEGA_Z (-0.025)

//-------+---------+---------+---------+---------+---------+---------+--

int main( int argc , char ** argv ) 
{
    if ( argc != 6 )
    {
        printf("usage: %s  input_file_prefix  R_EDGE  DELTAL  DELTAD  T\n", argv[0]);
        return(EXIT_FAILURE);
    }

#ifdef WORK_IN_ROTATING_FRAME
    printf("\n");
    printf("!!CAUTION!! WORK_IN_ROTATING_FRAME has been defined.\n");
    printf("!!CAUTION!! Coordinates will be transformed to the rotating frame: OMEGA_Z = %f\n", OMEGA_Z);
    printf("\n");
#else
    printf("\n");
    printf("!!CAUTION!! WORK_IN_ROTATING_FRAME has NOT been defined.\n");
    printf("\n");
#endif

    // Parameters for detecting vortex reconnections
    double R_EDGE, DELTAL, DELTAD;
    R_EDGE = atof(argv[2]);// Criterion for the displacement of ends of reconnecting vortices
    DELTAL = atof(argv[3]);// Criterion for the change of reconnecting vortices
    DELTAD = atof(argv[4]);// Criterion for the closest distance between reconnecting vortices
    //...

    //----------- Time setting -------------
    double time, dt, tstart, tcollide, t, eF=0.5;
    dt = 151.0060525 - 150.0060124;
    /* 2 knives (non-rotating frame) */
    // qt0dynh2
    //tstart=150.006012; tcollide=210.000000;
    // qt0dynr2
    //tstart=150.006012; tcollide=235.000000;
    // qt0dynr3
    //tstart=664.036047; tcollide=235.000000;
    //...

    /* 4 knives (non-rotating frame) */
    // qt0dynr2
    //tstart=199.007976; tcollide=260.000000;
    // qt1dynr2
    //tstart=111.004449; tcollide=220.000000;
    // qt1dynr3
    //tstart=509.029231; tcollide=220.000000;
    // qt2dynr2
    //tstart=124.004970; tcollide=220.000000;
    //...

    /* 4 knives (rotating frame) */
    // qt0dynr2rf
    tstart=149.005972; tcollide=235.000000;
    //...
    // for non-rotating frame coordinates should be transformed to the rotating frame
    //--------------------------------------

    // Variables
    int i, j, k, n, inom, nom, ivor, jvor, kvor, lvor, Nvor, Nvor_prev;
    int ielem, jelem, kelem, lelem, *Nelem, *Nelem_prev, *counter;
    double x, y, z, x1, y1, z1, x2, y2, z2, Ltot, dL, d, d_min, d_min_prev;
    double *Lvor, *Lvor_prev, *x_min, *x_max, *y_min, *y_max, *z_min, *z_max;
    double *x_min_prev, *y_min_prev, *z_min_prev;
    double *x_max_prev, *y_max_prev, *z_max_prev;
    double *xi_1, *yi_1, *zi_1, *xi_2, *yi_2, *zi_2;
    double *xj_1, *yj_1, *zj_1, *xj_2, *yj_2, *zj_2;
    double *xk_1, *yk_1, *zk_1, *xk_2, *yk_2, *zk_2;
    double *xl_1, *yl_1, *zl_1, *xl_2, *yl_2, *zl_2;
    int ip, ierr;
    double _x1, _y1, _x2, _y2, tmp;// Temporal use only
    char si_1[128], sj_1[128], sk_1[128], sl_1[128];
    char si_2[128], sj_2[128], sk_2[128], sl_2[128];

    // Set filenames
    char file_in[512], file_out[512], file_vis[512];
    sprintf(file_in ,"%s_vortex.elements", argv[1]);
    sprintf(file_out,"%s_reconnections.dat", argv[1],argv[2],argv[3],argv[4]);
    sprintf(file_vis,"%s_visualization.dat", argv[1],argv[2],argv[3],argv[4]);

    FILE *fp_in = fopen(file_in,"r");
    char buff[100];
    int line, total_lines, total_lines_prev;
    fgets(buff,100,fp_in); line=strlen(buff);

    FILE *fp_out = fopen(file_out,"w");
    fprintf(fp_out, "# SETTINGS:  R_EDGE: %s, DELTAL: %s, DELTAD: %s, T: %s", argv[2],argv[3],argv[4],argv[5]);
    fprintf(fp_out, "# idx   time   Nvor   Ltot   # of reconnections\n");
    fclose(fp_out);

    FILE *fp_vis = fopen(file_vis,"w");
    fprintf(fp_vis, "# x_i y_i z_i,  x_j y_j z_j,  x_k y_k z_k,  x_l y_l z_l,  time*eF  # of reconnections\n");
    fprintf(fp_vis, "# inom=0 (time*eF=%f)\n", tstart-tcollide);
    strcpy(si_1,"        -        -        -");
    fprintf(fp_vis, "%s %s %s %s  %f  0\n\n\n\n", si_1, si_1, si_1, si_1, tstart-tcollide);
    fclose(fp_vis);

    // Find nom
    fseek(fp_in,-line,SEEK_END);
    fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &j, &x1, &y1, &z1, &x2, &y2, &z2); nom=i;
    fseek(fp_in,0,SEEK_SET);
    cppmalloc(counter,nom,int); counter[0]=0;

    ///// Prepare "prev" from the initial frame /////
    // Find Nvor
    inom=0; time=tstart+inom*dt-tcollide; t=time/eF;
    do {
      fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &ivor, &x1, &y1, &z1, &x2, &y2, &z2);
      if(i==inom) Nvor=ivor+1;
    } while(i<=inom);
    fseek(fp_in,0,SEEK_SET);

    // Figure out length & edges of each vortex line
    cppmalloc( Lvor,Nvor,double); cppmalloc(Nelem,Nvor,int);
    cppmalloc(x_min,Nvor,double); cppmalloc(x_max,Nvor,double);
    cppmalloc(y_min,Nvor,double); cppmalloc(y_max,Nvor,double);
    cppmalloc(z_min,Nvor,double); cppmalloc(z_max,Nvor,double);
    for(ivor=0;ivor<Nvor;ivor++){ Lvor[ivor]=0.0; z_min[ivor]=100.0; z_max[ivor]=0.0; Nelem[ivor]=0; }//initialize
    k=0; do {
      fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &ivor, &x1, &y1, &z1, &x2, &y2, &z2); k++;
      if(i==inom)
      {
	#ifdef WORK_IN_ROTATING_FRAME // Transform to the rotating frame
	  _x1 = x1*cos(OMEGA_Z*t) - y1*sin(OMEGA_Z*t); _x2 = x2*cos(OMEGA_Z*t) - y2*sin(OMEGA_Z*t);
	  _y1 = x1*sin(OMEGA_Z*t) + y1*cos(OMEGA_Z*t); _y2 = x2*sin(OMEGA_Z*t) + y2*cos(OMEGA_Z*t);
	  x1  =_x1; y1=_y1; x2=_x2; y2=_y2;
	#endif
	x=x2-x1; y=y2-y1; z=z2-z1;
	Lvor[ivor] += sqrt(x*x+y*y+z*z); Nelem[ivor]+=1;
	if(z1<z_min[ivor]){ x_min[ivor]=x1; y_min[ivor]=y1; z_min[ivor]=z1; }// place of the  lowest end
	if(z2>z_max[ivor]){ x_max[ivor]=x2; y_max[ivor]=y2; z_max[ivor]=z2; }// place of the highest end
      }
    } while(i<=inom); total_lines=k-1;
    fseek(fp_in,-line,SEEK_CUR);

    // Store "prev"
    cppmalloc( Lvor_prev,Nvor,double); cppmalloc(Nelem_prev,Nvor,int);
    cppmalloc(x_min_prev,Nvor,double); cppmalloc(x_max_prev,Nvor,double);
    cppmalloc(y_min_prev,Nvor,double); cppmalloc(y_max_prev,Nvor,double);
    cppmalloc(z_min_prev,Nvor,double); cppmalloc(z_max_prev,Nvor,double);
    for(ivor=0;ivor<Nvor;ivor++){
       Lvor_prev[ivor]= Lvor[ivor]; Nelem_prev[ivor]=Nelem[ivor];
      x_min_prev[ivor]=x_min[ivor]; x_max_prev[ivor]=x_max[ivor];
      y_min_prev[ivor]=y_min[ivor]; y_max_prev[ivor]=y_max[ivor];
      z_min_prev[ivor]=z_min[ivor]; z_max_prev[ivor]=z_max[ivor];
    }
    Nvor_prev=Nvor; total_lines_prev=total_lines;

    // Clean for the next
    free(Lvor); free(Nelem); free(x_min); free(y_min); free(z_min); free(x_max); free(y_max); free(z_max);
    //...


    //................................... Loop over measurements ...................................
    for(inom=1; inom<nom; inom++)
    {
      time=tstart+inom*dt-tcollide; t=time/eF;

      // Find Nvor
      k=0; do {
	fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &ivor, &x1, &y1, &z1, &x2, &y2, &z2); k++;
	if(i==inom) Nvor=ivor+1;
      } while(i<=inom);
      fseek(fp_in,-k*line,SEEK_CUR);

      // Figure out length & edges & Nelem of each vortex line
      cppmalloc( Lvor,Nvor,double); cppmalloc(Nelem,Nvor,int);
      cppmalloc(x_min,Nvor,double); cppmalloc(x_max,Nvor,double);
      cppmalloc(y_min,Nvor,double); cppmalloc(y_max,Nvor,double);
      cppmalloc(z_min,Nvor,double); cppmalloc(z_max,Nvor,double);
      for(ivor=0;ivor<Nvor;ivor++){ Lvor[ivor]=0.0; z_min[ivor]=100.0; z_max[ivor]=0.0; Nelem[ivor]=0; }//initialize
      k=0; do {
	fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &ivor, &x1, &y1, &z1, &x2, &y2, &z2); k++;
	if(i==inom)
	{
          #ifdef WORK_IN_ROTATING_FRAME // Transform to the rotating frame
	    _x1 = x1*cos(OMEGA_Z*t) - y1*sin(OMEGA_Z*t); _x2 = x2*cos(OMEGA_Z*t) - y2*sin(OMEGA_Z*t);
	    _y1 = x1*sin(OMEGA_Z*t) + y1*cos(OMEGA_Z*t); _y2 = x2*sin(OMEGA_Z*t) + y2*cos(OMEGA_Z*t);
	    x1=_x1; y1=_y1; x2=_x2; y2=_y2;
          #endif
	  x=x2-x1; y=y2-y1; z=z2-z1;
	  Lvor[ivor] += sqrt(x*x+y*y+z*z); Nelem[ivor]+=1;
	  if(z1<z_min[ivor]){ x_min[ivor]=x1; y_min[ivor]=y1; z_min[ivor]=z1; }// place of the  lowest end
	  if(z2>z_max[ivor]){ x_max[ivor]=x2; y_max[ivor]=y2; z_max[ivor]=z2; }// place of the highest end
	}
      } while(i<=inom); total_lines=k-1;
      Ltot=0.0; for(ivor=0;ivor<Nvor;ivor++) Ltot+=Lvor[ivor];
      fseek(fp_in,-k*line,SEEK_CUR);


      //_/_/_/_/_/_/_/_/_/_/_/_/_/_/ Search for reconnections _/_/_/_/_/_/_/_/_/_/_/_/_/_/
      counter[inom]=0;
      for(ivor=0;ivor<Nvor     ;ivor++)// ivor - index for current  time
      for(kvor=0;kvor<Nvor_prev;kvor++)// kvor - index for previous time
      {
	// Check if there is a vortex line with the same ends
	x1=x_min[ivor]-x_min_prev[kvor]; x2=x_max[ivor]-x_max_prev[kvor];
	y1=y_min[ivor]-y_min_prev[kvor]; y2=y_max[ivor]-y_max_prev[kvor];
	z1=z_min[ivor]-z_min_prev[kvor]; z2=z_max[ivor]-z_max_prev[kvor];
	if(sqrt(x1*x1+y1*y1+z1*z1) < R_EDGE && sqrt(x2*x2+y2*y2+z2*z2) < R_EDGE) continue;// ivor = kvor -> No reconnection
	//...

	if(sqrt(x1*x1+y1*y1+z1*z1) < R_EDGE)// "ivor and kvor have the same lower end"
	{
	  for(lvor=0;lvor<Nvor_prev;lvor++)// lvor - index for previous time
	  {
	    if(lvor==kvor) continue;
	    x2=x_max[ivor]-x_max_prev[lvor];
	    y2=y_max[ivor]-y_max_prev[lvor];
	    z2=z_max[ivor]-z_max_prev[lvor];
	    if(sqrt(x2*x2+y2*y2+z2*z2) < R_EDGE)// "ivor and lvor have the same upper end"
	    {
	      for(jvor=ivor+1;jvor<Nvor;jvor++)// jvor - index for current time
	      {
		x1=x_min[jvor]-x_min_prev[lvor]; x2=x_max[jvor]-x_max_prev[kvor];
		y1=y_min[jvor]-y_min_prev[lvor]; y2=y_max[jvor]-y_max_prev[kvor];
		z1=z_min[jvor]-z_min_prev[lvor]; z2=z_max[jvor]-z_max_prev[kvor];
		if(sqrt(x1*x1+y1*y1+z1*z1) < R_EDGE && sqrt(x2*x2+y2*y2+z2*z2) < R_EDGE)// Here is a candidate://  prev ->  now  //
		{                                                                                              //  l  k    i  j  //
		  dL = abs(Lvor[ivor]+Lvor[jvor]-Lvor_prev[kvor]-Lvor_prev[lvor]);                             //   \/      \/   //
		  if(dL < DELTAL)// Total length should not differ much before/after the reconnection          //   /\      /\   //
		  {                                                                                            //  k  l    i  j  //
		    // Get coordinates of all elements of ivor & jvor
		    cppmalloc(xi_1,Nelem[ivor],double); cppmalloc(xj_1,Nelem[jvor],double);
		    cppmalloc(yi_1,Nelem[ivor],double); cppmalloc(yj_1,Nelem[jvor],double);
		    cppmalloc(zi_1,Nelem[ivor],double); cppmalloc(zj_1,Nelem[jvor],double);
		    cppmalloc(xi_2,Nelem[ivor],double); cppmalloc(xj_2,Nelem[jvor],double);
		    cppmalloc(yi_2,Nelem[ivor],double); cppmalloc(yj_2,Nelem[jvor],double);
		    cppmalloc(zi_2,Nelem[ivor],double); cppmalloc(zj_2,Nelem[jvor],double);
		    ielem=0; jelem=0;
		    k=0; do {
		      fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &j, &x1, &y1, &z1, &x2, &y2, &z2); k++;
		      if(i==inom)
		      {
                        #ifdef WORK_IN_ROTATING_FRAME // Transform to the rotating frame
			  _x1 = x1*cos(OMEGA_Z*t) - y1*sin(OMEGA_Z*t); _x2 = x2*cos(OMEGA_Z*t) - y2*sin(OMEGA_Z*t);
			  _y1 = x1*sin(OMEGA_Z*t) + y1*cos(OMEGA_Z*t); _y2 = x2*sin(OMEGA_Z*t) + y2*cos(OMEGA_Z*t);
			  x1=_x1; y1=_y1; x2=_x2; y2=_y2;
			#endif
			if(j==ivor){
			  xi_1[ielem]=x1; yi_1[ielem]=y1; zi_1[ielem]=z1;
			  xi_2[ielem]=x2; yi_2[ielem]=y2; zi_2[ielem]=z2;
			  ielem+=1;
			}
			if(j==jvor){
			  xj_1[jelem]=x1; yj_1[jelem]=y1; zj_1[jelem]=z1;
			  xj_2[jelem]=x2; yj_2[jelem]=y2; zj_2[jelem]=z2;
			  jelem+=1;
			}
			if(ielem==Nelem[ivor] && jelem==Nelem[jvor]) break;
		      }
		    } while(i<=inom);
		    fseek(fp_in,-k*line,SEEK_CUR);

		    // Get coordinates of all elements of kvor & lvor
		    fseek(fp_in,-total_lines_prev*line,SEEK_CUR);//goto the previous frame
		    cppmalloc(xk_1,Nelem_prev[kvor],double); cppmalloc(xl_1,Nelem_prev[lvor],double);
		    cppmalloc(yk_1,Nelem_prev[kvor],double); cppmalloc(yl_1,Nelem_prev[lvor],double);
		    cppmalloc(zk_1,Nelem_prev[kvor],double); cppmalloc(zl_1,Nelem_prev[lvor],double);
		    cppmalloc(xk_2,Nelem_prev[kvor],double); cppmalloc(xl_2,Nelem_prev[lvor],double);
		    cppmalloc(yk_2,Nelem_prev[kvor],double); cppmalloc(yl_2,Nelem_prev[lvor],double);
		    cppmalloc(zk_2,Nelem_prev[kvor],double); cppmalloc(zl_2,Nelem_prev[lvor],double);
		    kelem=0; lelem=0;
		    k=0; do {
		      fscanf(fp_in, "%d %d %lf %lf %lf %lf %lf %lf", &i, &j, &x1, &y1, &z1, &x2, &y2, &z2); k++;
		      if(i==inom-1)
		      {
                        #ifdef WORK_IN_ROTATING_FRAME // Transform to the rotating frame
			  _x1 = x1*cos(OMEGA_Z*(t-dt/eF)) - y1*sin(OMEGA_Z*(t-dt/eF)); _x2 = x2*cos(OMEGA_Z*(t-dt/eF)) - y2*sin(OMEGA_Z*(t-dt/eF));
			  _y1 = x1*sin(OMEGA_Z*(t-dt/eF)) + y1*cos(OMEGA_Z*(t-dt/eF)); _y2 = x2*sin(OMEGA_Z*(t-dt/eF)) + y2*cos(OMEGA_Z*(t-dt/eF));
			  x1=_x1; y1=_y1; x2=_x2; y2=_y2;
			#endif
			if(j==kvor){
			  xk_1[kelem]=x1; yk_1[kelem]=y1; zk_1[kelem]=z1;
			  xk_2[kelem]=x2; yk_2[kelem]=y2; zk_2[kelem]=z2;
			  kelem+=1;
			}
			if(j==lvor){
			  xl_1[lelem]=x1; yl_1[lelem]=y1; zl_1[lelem]=z1;
			  xl_2[lelem]=x2; yl_2[lelem]=y2; zl_2[lelem]=z2;
			  lelem+=1;
			}
			if(kelem==Nelem_prev[kvor] && lelem==Nelem_prev[lvor]) break;
		      }
		    } while(i<=inom-1);
		    fseek(fp_in,(total_lines_prev-k)*line,SEEK_CUR);//goto the current frame

		    // Find the closest distance between the pairs: ivor & jvor, kvor & lvor
		    d_min=100.0;
		    for(ielem=0;ielem<Nelem[ivor];ielem++)
		    for(jelem=0;jelem<Nelem[jvor];jelem++)
		    {
		      x=xj_1[jelem]-xi_1[ielem]; y=yj_1[jelem]-yi_1[ielem]; z=zj_1[jelem]-zi_1[ielem]; d=sqrt(x*x+y*y+z*z); if(d<d_min) d_min=d;
		      x=xj_2[jelem]-xi_1[ielem]; y=yj_2[jelem]-yi_1[ielem]; z=zj_2[jelem]-zi_1[ielem]; d=sqrt(x*x+y*y+z*z); if(d<d_min) d_min=d;
		      x=xj_1[jelem]-xi_2[ielem]; y=yj_1[jelem]-yi_2[ielem]; z=zj_1[jelem]-zi_2[ielem]; d=sqrt(x*x+y*y+z*z); if(d<d_min) d_min=d;
		      x=xj_2[jelem]-xi_2[ielem]; y=yj_2[jelem]-yi_2[ielem]; z=zj_2[jelem]-zi_2[ielem]; d=sqrt(x*x+y*y+z*z); if(d<d_min) d_min=d;
		    }
		    d_min_prev=100.0;
		    for(kelem=0;kelem<Nelem_prev[kvor];kelem++)
		    for(lelem=0;lelem<Nelem_prev[lvor];lelem++)
		    {
		      x=xl_1[lelem]-xk_1[kelem]; y=yl_1[lelem]-yk_1[kelem]; z=zl_1[lelem]-zk_1[kelem]; d=sqrt(x*x+y*y+z*z); if(d<d_min_prev) d_min_prev=d;
		      x=xl_2[lelem]-xk_1[kelem]; y=yl_2[lelem]-yk_1[kelem]; z=zl_2[lelem]-zk_1[kelem]; d=sqrt(x*x+y*y+z*z); if(d<d_min_prev) d_min_prev=d;
		      x=xl_1[lelem]-xk_2[kelem]; y=yl_1[lelem]-yk_2[kelem]; z=zl_1[lelem]-zk_2[kelem]; d=sqrt(x*x+y*y+z*z); if(d<d_min_prev) d_min_prev=d;
		      x=xl_2[lelem]-xk_2[kelem]; y=yl_2[lelem]-yk_2[kelem]; z=zl_2[lelem]-zk_2[kelem]; d=sqrt(x*x+y*y+z*z); if(d<d_min_prev) d_min_prev=d;
		    }

		    // Final judgement
		    if(d_min < DELTAD && d_min_prev < DELTAD)// Each pair (ivor & jvor, kvor & lvor) should be close enough
		    {
		      counter[inom]+=1;// We found a reconnection!

		      //printf("!!TEST!! inom: %d:  ivor = %d, jvor = %d, kvor = %d, lvor = %d\n", inom, ivor, jvor, kvor, lvor);

		      // Sort in ascending order w.r.t. "z" for visualization
		      // ivor
		      for(i=0  ;i<Nelem[ivor];i++)
		      for(j=i+1;j<Nelem[ivor];j++)
		      {
			if(zi_1[i]>zi_1[j]){
			  tmp=xi_1[i]; xi_1[i]=xi_1[j]; xi_1[j]=tmp;
			  tmp=yi_1[i]; yi_1[i]=yi_1[j]; yi_1[j]=tmp;
			  tmp=zi_1[i]; zi_1[i]=zi_1[j]; zi_1[j]=tmp;
			  tmp=xi_2[i]; xi_2[i]=xi_2[j]; xi_2[j]=tmp;
			  tmp=yi_2[i]; yi_2[i]=yi_2[j]; yi_2[j]=tmp;
			  tmp=zi_2[i]; zi_2[i]=zi_2[j]; zi_2[j]=tmp;
			}
		      }
		      // jvor
		      for(i=0  ;i<Nelem[jvor];i++)
		      for(j=i+1;j<Nelem[jvor];j++)
		      {
			if(zj_1[i]>zj_1[j]){
			  tmp=xj_1[i]; xj_1[i]=xj_1[j]; xj_1[j]=tmp;
			  tmp=yj_1[i]; yj_1[i]=yj_1[j]; yj_1[j]=tmp;
			  tmp=zj_1[i]; zj_1[i]=zj_1[j]; zj_1[j]=tmp;
			  tmp=xj_2[i]; xj_2[i]=xj_2[j]; xj_2[j]=tmp;
			  tmp=yj_2[i]; yj_2[i]=yj_2[j]; yj_2[j]=tmp;
			  tmp=zj_2[i]; zj_2[i]=zj_2[j]; zj_2[j]=tmp;
			}
		      }
		      // kvor
		      for(i=0  ;i<Nelem_prev[kvor];i++)
		      for(j=i+1;j<Nelem_prev[kvor];j++)
		      {
			if(zk_1[i]>zk_1[j]){
			  tmp=xk_1[i]; xk_1[i]=xk_1[j]; xk_1[j]=tmp;
			  tmp=yk_1[i]; yk_1[i]=yk_1[j]; yk_1[j]=tmp;
			  tmp=zk_1[i]; zk_1[i]=zk_1[j]; zk_1[j]=tmp;
			  tmp=xk_2[i]; xk_2[i]=xk_2[j]; xk_2[j]=tmp;
			  tmp=yk_2[i]; yk_2[i]=yk_2[j]; yk_2[j]=tmp;
			  tmp=zk_2[i]; zk_2[i]=zk_2[j]; zk_2[j]=tmp;
			}
		      }
		      // lvor
		      for(i=0  ;i<Nelem_prev[lvor];i++)
		      for(j=i+1;j<Nelem_prev[lvor];j++)
		      {
			if(zl_1[i]>zl_1[j]){
			  tmp=xl_1[i]; xl_1[i]=xl_1[j]; xl_1[j]=tmp;
			  tmp=yl_1[i]; yl_1[i]=yl_1[j]; yl_1[j]=tmp;
			  tmp=zl_1[i]; zl_1[i]=zl_1[j]; zl_1[j]=tmp;
			  tmp=xl_2[i]; xl_2[i]=xl_2[j]; xl_2[j]=tmp;
			  tmp=yl_2[i]; yl_2[i]=yl_2[j]; yl_2[j]=tmp;
			  tmp=zl_2[i]; zl_2[i]=zl_2[j]; zl_2[j]=tmp;
			}
		      }

		      // OUTPUT for visualization
		      if(Nelem[ivor]>Nelem[jvor]) i=Nelem[ivor]; else i=Nelem[jvor];
		      if(Nelem[kvor]>Nelem[lvor]) k=Nelem[kvor]; else k=Nelem[lvor];
		      if(i>k) n=i; else n=k;// n: largest Nelem among ivor,jvor,kvor,lvor

		      fp_vis = fopen(file_vis,"a");
		      if(counter[inom]==1) fprintf(fp_vis, "# inom=%d (time*eF=%f)\n", inom, time);
		      for(i=0;i<n;i++)
		      {
			if(i<Nelem[ivor]){
			  sprintf(si_1, "%f %f %f", xi_1[i], yi_1[i], zi_1[i]);
			  sprintf(si_2, "%f %f %f", xi_2[i], yi_2[i], zi_2[i]);
			} else {
			  strcpy(si_1,"        -        -        -");
			  strcpy(si_2,"        -        -        -");
			}
			if(i<Nelem[jvor]){
			  sprintf(sj_1, "%f %f %f", xj_1[i], yj_1[i], zj_1[i]);
			  sprintf(sj_2, "%f %f %f", xj_2[i], yj_2[i], zj_2[i]);
			} else {
			  strcpy(sj_1,"        -        -        -");
			  strcpy(sj_2,"        -        -        -");
			}
			if(i<Nelem_prev[kvor]){
			  sprintf(sk_1, "%f %f %f", xk_1[i], yk_1[i], zk_1[i]);
			  sprintf(sk_2, "%f %f %f", xk_2[i], yk_2[i], zk_2[i]);
			} else {
			  strcpy(sk_1,"        -        -        -");
			  strcpy(sk_2,"        -        -        -");
			}
			if(i<Nelem_prev[lvor]){
			  sprintf(sl_1, "%f %f %f", xl_1[i], yl_1[i], zl_1[i]);
			  sprintf(sl_2, "%f %f %f", xl_2[i], yl_2[i], zl_2[i]);
			} else {
			  strcpy(sl_1,"        -        -        -");
			  strcpy(sl_2,"        -        -        -");
			}
			fprintf(fp_vis, "%s %s %s %s  %f  %d\n", si_1, sj_1, sk_1, sl_1, time, counter[inom]);
			fprintf(fp_vis, "%s %s %s %s  %f  %d\n", si_2, sj_2, sk_2, sl_2, time, counter[inom]);
		      }
		      fclose(fp_vis);
		    }
		    free(xi_1); free(yi_1); free(zi_1); free(xi_2); free(yi_2); free(zi_2);
		    free(xj_1); free(yj_1); free(zj_1); free(xj_2); free(yj_2); free(zj_2);
		    free(xk_1); free(yk_1); free(zk_1); free(xk_2); free(yk_2); free(zk_2);
		    free(xl_1); free(yl_1); free(zl_1); free(xl_2); free(yl_2); free(zl_2);
		  }
		}
	      }
	    }
	  }
	}
      }// loop over ivor & kvor

      // Report
      printf("reconnections> t=%f  Nvor=%d  Ltot=%f:  %d\n", time, Nvor, Ltot, counter[inom]);

      // Output
      fp_out = fopen(file_out,"a");
      fprintf(fp_out, "%d  %f  %d  %f  %d\n", inom, time, Nvor, Ltot, counter[inom]);
      fclose(fp_out);

      // For visualization
      fp_vis = fopen(file_vis,"a");
      if(counter[inom]==0)
      {
	fprintf(fp_vis, "# inom=%d (time*eF=%f):  No reconnection detected.\n", inom, time);
	strcpy(si_1,"        -        -        -");
	fprintf(fp_vis, "%s %s %s %s  %f  %d\n", si_1, si_1, si_1, si_1, time, counter[inom]);
      }
      fprintf(fp_vis, "\n\n\n");
      fclose(fp_vis);

      // Update "prev"
      free( Lvor_prev); free(x_min_prev); free(y_min_prev); free(z_min_prev);
      free(Nelem_prev); free(x_max_prev); free(y_max_prev); free(z_max_prev);
      cppmalloc( Lvor_prev,Nvor,double); cppmalloc(x_min_prev,Nvor,double); cppmalloc(y_min_prev,Nvor,double); cppmalloc(z_min_prev,Nvor,double);
      cppmalloc(Nelem_prev,Nvor,int   ); cppmalloc(x_max_prev,Nvor,double); cppmalloc(y_max_prev,Nvor,double); cppmalloc(z_max_prev,Nvor,double);
      for(ivor=0;ivor<Nvor;ivor++){
	 Lvor_prev[ivor]= Lvor[ivor]; Nelem_prev[ivor]=Nelem[ivor];
	x_min_prev[ivor]=x_min[ivor]; x_max_prev[ivor]=x_max[ivor];
	y_min_prev[ivor]=y_min[ivor]; y_max_prev[ivor]=y_max[ivor];
	z_min_prev[ivor]=z_min[ivor]; z_max_prev[ivor]=z_max[ivor];
      }
      Nvor_prev=Nvor; total_lines_prev=total_lines;

      // Clean for the next
      free(Lvor); free(Nelem); free(x_min); free(y_min); free(z_min); free(x_max); free(y_max); free(z_max);
    }// loop over inom
    fclose(fp_in);
    //.....


    // Take an average over certain time interval
    int iT, isum;
    double T = atof(argv[5]), Gaussians;

    char file_sum[512];
    sprintf(file_sum,"%s_reconnections_ave.dat", argv[1]);
    FILE *fp_sum = fopen(file_sum,"w");
    fprintf(fp_sum, "# SETTINGS:  R_EDGE: %s, DELTAL: %s, DELTAD: %s, T: %s", argv[2],argv[3],argv[4],argv[5]);
    fprintf(fp_sum, "# time,  # of reconnections,  integrated over a time interval, T = %s,  averaged # per measurement,  Gaussians\n",argv[5]);

    iT = (int)(T/dt + 0.5);//rounding off
    for(inom=iT+1; inom<nom-iT; inom++)
    {
      time=tstart+inom*dt-tcollide;
      isum=0; for(i=inom-iT; i<inom+iT; i++) isum += counter[i];
      Gaussians=0.0; for(i=0;i<nom;i++){ t=tstart+i*dt-tcollide; Gaussians += (double)(counter[i])*exp(-pow(t-time,2)/(2*T*T)); }
      fprintf(fp_sum, "%f  %d  %d  %f  %f\n", time, counter[inom], isum, isum/(double)(2*iT+1), Gaussians);
    }
    fclose(fp_sum); free(counter);
    //...

    return(EXIT_SUCCESS);
}

//-------+---------+---------+---------+---------+---------+---------+--
